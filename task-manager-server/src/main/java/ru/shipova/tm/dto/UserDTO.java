package ru.shipova.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.constant.RoleType;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class UserDTO extends AbstractEntityDTO implements Serializable {
    public static final long serialVersionUID = 1;

    @Nullable
    private String login;

    @Nullable
    private RoleType roleType;

    @Nullable
    private String passwordHash;

    public UserDTO(@NotNull final String id, @Nullable final String login, @Nullable final String passwordHash, @Nullable final RoleType roleType) {
        this.id = id;
        this.login = login;
        this.roleType = roleType;
        this.passwordHash = passwordHash;
    }
}
