package ru.shipova.tm.serializer;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.ISerializer;
import ru.shipova.tm.constant.DataConstant;
import ru.shipova.tm.dto.DomainDTO;

import java.io.*;

public class BinarySerializer implements ISerializer {
    @Override
    public void serialize(@NotNull final DomainDTO domainDTO) throws IOException {
        @NotNull final File file = new File(DataConstant.FILE_BINARY.displayName());
        try (@NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
             @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(domainDTO);
        }
    }

    @Override
    @Nullable
    public DomainDTO deserialize(){
        @NotNull final File file = new File(DataConstant.FILE_BINARY.displayName());
        @Nullable DomainDTO domainDTO = null;
        try(@NotNull final FileInputStream fileInputStream = new FileInputStream(file);
            @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
           domainDTO = (DomainDTO) objectInputStream.readObject();
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return domainDTO;
    }
}
