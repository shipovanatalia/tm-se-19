package ru.shipova.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.entity.Session;

public interface IAdminUserService {
    boolean getAccessToUsualCommand(@Nullable final Session session, boolean needAuthorize);
    boolean getAccessToAdminCommand(@Nullable final Session session, boolean isOnlyAdminCommand);
}
