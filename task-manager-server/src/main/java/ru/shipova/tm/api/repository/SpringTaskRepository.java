package ru.shipova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.shipova.tm.entity.Task;

import java.util.List;

@Repository
public interface SpringTaskRepository extends JpaRepository<Task, String> {
    void deleteAllByProjectId(@NotNull final String projectId);

    void deleteAllByUserId(@NotNull final String userId);

    void deleteByUserIdAndName(@NotNull final String userId, @NotNull final String name);

    @NotNull List<Task> findAllByProjectId(@NotNull final String projectId);

    @NotNull List<Task> findAllByUserId (@NotNull final String userId);

    @NotNull
    @Query("select p from Project p where user.id = :userId and name like %:partOfData%")
    List<Task> findAllByPartOfData(@Param("userId") @NotNull final String userId,
                                      @Param("partOfData") @NotNull final String partOfData);
}
