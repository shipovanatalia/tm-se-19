package ru.shipova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.shipova.tm.entity.Project;

import java.util.List;

@Repository
public interface SpringProjectRepository extends JpaRepository<Project, String> {
    @NotNull List<Project> findAllByUserId(@NotNull final String userId);

    @NotNull Project findByName(@NotNull final String projectName);

    @NotNull
    @Query("select p from Project p where user.id = :userId and name like %:partOfData%")
    List<Project> findAllByPartOfData(@Param("userId") @NotNull final String userId,
                                 @Param("partOfData") @NotNull final String partOfData);

    void deleteAllByUserId(@NotNull final String userId);

    void deleteByUserIdAndName(@NotNull final String userId, @NotNull final String projectName);
}
