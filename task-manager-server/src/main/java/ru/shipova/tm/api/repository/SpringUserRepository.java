package ru.shipova.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.shipova.tm.entity.User;

@Repository
public interface SpringUserRepository extends JpaRepository<User, String> {
    @Nullable User findByLogin(String login);
}
