package ru.shipova.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.shipova.tm.entity.Session;

@Repository
public interface SpringSessionRepository extends JpaRepository<Session, String> {
}
