package ru.shipova.tm.api;

import org.jetbrains.annotations.NotNull;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import ru.shipova.tm.api.service.*;

import javax.persistence.EntityManagerFactory;

public interface IServiceLocator {
    @NotNull IProjectService getIProjectService();
    @NotNull ITaskService getITaskService();
    @NotNull IUserService getIUserService();
    @NotNull IDomainService getIDomainService();
    @NotNull ISessionService getISessionService();
    @NotNull IAdminUserService getIAdminUserService();
    EntityManagerFactory getEntityManagerFactory();
}
