package ru.shipova.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.shipova.tm.bootstrap.Bootstrap;
import ru.shipova.tm.config.SpringConfig;


public final class ServerApplication {

    private static AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

    public static void main(String[] args) {
        context.register(SpringConfig.class);
        context.scan("ru.shipova.tm");
        context.refresh();
        @NotNull final  Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init();
    }
}
