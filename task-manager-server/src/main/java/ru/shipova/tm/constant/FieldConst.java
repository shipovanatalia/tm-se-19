package ru.shipova.tm.constant;


public class FieldConst {
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String DATE_CREATE = "dateCreate";
    public static final String DATE_BEGIN = "dateBegin";
    public static final String DATE_END = "dateEnd";
    public static final String USER_ID = "user_id";
    public static final String STATUS = "status";
    public static final String LOGIN = "login";
    public static final String PASSWORD_HASH = "passwordHash";
    public static final String ROLE_TYPE = "role";
    public static final String PROJECT_ID = "project_Id";
    public static final String SIGNATURE = "signature";
    public static final String TIMESTAMP = "timestamp";
}
