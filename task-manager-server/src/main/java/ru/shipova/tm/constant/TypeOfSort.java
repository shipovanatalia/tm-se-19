package ru.shipova.tm.constant;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum TypeOfSort {
    NO("NO"),
    DATE_OF_CREATE("DATE OF CREATE"),
    DATE_OF_BEGIN("DATE OF BEGIN"),
    DATE_OF_END("DATE OF END"),
    STATUS("STATUS");

    private @NotNull final String name;

    TypeOfSort(@NotNull final String name) {
        this.name = name;
    }

    @NotNull public String displayName() {
        return name;
    }

    @Nullable
    public static TypeOfSort getTypeOfSort(@Nullable final String displayName){
        for (TypeOfSort typeOfSort: TypeOfSort.values()) {
            if (typeOfSort.displayName().equals(displayName)){
                return typeOfSort;
            }
        }
        return null;
    }
}
