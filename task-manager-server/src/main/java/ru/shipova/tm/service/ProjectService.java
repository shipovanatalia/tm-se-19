package ru.shipova.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.shipova.tm.api.repository.SpringProjectRepository;
import ru.shipova.tm.api.repository.SpringTaskRepository;
import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.constant.Status;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.entity.User;

import javax.annotation.Nullable;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
@NoArgsConstructor
public final class ProjectService implements IProjectService {

    @Autowired
    @Nullable
    private SpringTaskRepository taskRepository;

    @Autowired
    @Nullable
    private SpringProjectRepository projectRepository;

    @Override
    @Nullable
    public List<Project> getProjectListOfUser(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public void load(@Nullable final List<Project> projectList) {
        if (projectList == null) return;
        for (@Nullable final Project project : projectList) {
            if (project == null) return;
            projectRepository.save(project);
        }
    }

    @Nullable
    @Override
    public List<Project> search(@Nullable final String userId, @Nullable final String partOfData) {
        if (userId == null || userId.isEmpty()) return null;
        if (partOfData == null || partOfData.isEmpty()) return null;
        return projectRepository.findAllByPartOfData(userId, partOfData);
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final String projectName) {
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setId(UUID.randomUUID().toString());
        @NotNull final User user = new User();
        user.setId(userId);
        project.setUser(user);
        project.setDateOfBegin(new Date());
        project.setStatus(Status.PLANNED);
        projectRepository.save(project);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectRepository.deleteAllByUserId(userId);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectName) {
        if (projectName == null || projectName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        projectRepository.deleteByUserIdAndName(userId, projectName);
    }
}
