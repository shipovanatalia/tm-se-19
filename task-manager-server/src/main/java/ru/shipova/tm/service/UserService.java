package ru.shipova.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.shipova.tm.api.repository.SpringUserRepository;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.LoginAlreadyExistsException;
import ru.shipova.tm.util.HashUtil;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Getter
@Setter
@Service
@Transactional
@NoArgsConstructor
public final class UserService implements IUserService {

    @Autowired
    private @Nullable SpringUserRepository userRepository;

    @Override
    public void createUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (roleType == null) return;
        @NotNull final String userId = UUID.randomUUID().toString();
        @Nullable final String passwordHash = HashUtil.md5(password);
        if (passwordHash == null) return;
        @NotNull final User user = new User(userId, login, passwordHash, roleType);
        userRepository.save(user);
    }

    @Override
    public void registryUser(@Nullable final String login, @Nullable final String password, @Nullable final String role) throws LoginAlreadyExistsException {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (role == null || role.isEmpty()) return;
        if (userRepository.findByLogin(login) != null) throw new LoginAlreadyExistsException();
        @Nullable final String passwordHash = HashUtil.md5(password);
        @NotNull final String id = UUID.randomUUID().toString();
        @Nullable final RoleType roleType = getRoleType(role);
        userRepository.save(new User(id, login, passwordHash, roleType));
    }

    @Nullable
    @Override
    public User authorize(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) return null;
        if (password.isEmpty()) return null;
        @Nullable User user = userRepository.findByLogin(login);
        if (user == null) return null;
        @Nullable final String passwordHash = HashUtil.md5(password);
        if (passwordHash == null) return null;
        if (passwordHash.equals(user.getPasswordHash())) {
            return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User findById(@NotNull final String userId) {
        @Nullable final Optional<User> optional = userRepository.findById(userId);
        return optional.orElse(null);
    }

    @Nullable
    @Override
    public RoleType getRoleType(@NotNull final Session session) {
        @Nullable final User user = findById(session.getUser().getId());
        if (user == null) return null;
        @Nullable final RoleType roleType = user.getRoleType();
        return roleType;
    }

    @Nullable
    @Override
    public RoleType getRoleType(@Nullable final String role) {
        if (role == null || role.isEmpty()) return null;
        RoleType roleType = null;
        switch (role.toUpperCase()) {
            case "USER":
                roleType = RoleType.USER;
                break;
            case "ADMIN":
                roleType = RoleType.ADMIN;
                break;
        }
        return roleType;
    }

    @Override
    public void updateUser(@Nullable final String login, @Nullable final String role) {
        if (role == null || role.isEmpty()) return;
        if (login == null || login.isEmpty()) return;
        @Nullable final RoleType roleType = getRoleType(role);
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return;
        user.setRoleType(roleType);
        user.setLogin(login);
        user.setRoleType(roleType);
        userRepository.save(user);
    }

    @Override
    public void setNewPassword(@Nullable final String login, @NotNull final String password) {
        if (password.isEmpty()) return;
        if (login == null || login.isEmpty()) return;
        @Nullable final String passwordHash = HashUtil.md5(password);
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return;
        user.setPasswordHash(passwordHash);
        userRepository.save(user);
    }

    @Override
    @NotNull
    public List<User> getUserList() {
        return userRepository.findAll();
    }

    @Override
    public void load(@Nullable final List<User> userList) {
        if (userList == null) return;
        for (@Nullable final User user : userList) {
            if (user == null) return;
            userRepository.save(user);
        }
    }
}