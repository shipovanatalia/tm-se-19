package ru.shipova.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.shipova.tm.api.repository.SpringProjectRepository;
import ru.shipova.tm.api.repository.SpringTaskRepository;
import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.constant.Status;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.exception.ProjectDoesNotExistException;

import javax.annotation.Nullable;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
@NoArgsConstructor
public final class TaskService implements ITaskService {
    @Autowired
    @Nullable
    private SpringTaskRepository taskRepository;

    @Autowired
    @Nullable
    private SpringProjectRepository projectRepository;

    @NotNull
    @Override
    public List<Task> showAllTasksOfProject(@NotNull final String projectName) throws ProjectDoesNotExistException {
        if (projectName.isEmpty()) throw new ProjectDoesNotExistException();
        @Nullable final Project project = projectRepository.findByName(projectName);
        @NotNull final String projectId = project.getId();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Nullable
    @Override
    public List<Task> getTaskListOfUser(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public void load(@Nullable final List<Task> taskList) {
        if (taskList == null) return;
        for (@Nullable final Task task : taskList) {
            if (task == null) return;
            taskRepository.save(task);
        }
    }

    @Nullable
    @Override
    public List<Task> search(@Nullable final String userId, @Nullable final String partOfData) {
        if (userId == null || userId.isEmpty()) return null;
        if (partOfData == null || partOfData.isEmpty()) return null;
        return taskRepository.findAllByPartOfData(userId, partOfData);
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String taskName, @Nullable final String projectName) throws ProjectDoesNotExistException {
        if (userId == null || userId.isEmpty()) return;
        if (taskName == null || taskName.isEmpty()) return;
        if (projectName == null || projectName.isEmpty()) return;
        @Nullable final Project project = projectRepository.findByName(projectName);
        @NotNull final String taskId = UUID.randomUUID().toString();
        @NotNull final Task task = new Task();
        task.setId(taskId);
        task.setName(taskName);
        task.setProject(project);
        task.setUser(project.getUser());
        task.setDateOfCreate(new Date());
        task.setStatus(Status.PLANNED);
        taskRepository.save(task);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        taskRepository.deleteAllByUserId(userId);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String taskName) {
        if (taskName == null || taskName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        taskRepository.deleteByUserIdAndName(userId, taskName);
    }
}
