package ru.shipova.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.shipova.tm.api.repository.SpringSessionRepository;
import ru.shipova.tm.api.service.ISessionService;
import ru.shipova.tm.dto.Result;
import ru.shipova.tm.dto.SessionDTO;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.AccessForbiddenException;
import ru.shipova.tm.util.EntityConvertUtil;
import ru.shipova.tm.util.SignatureUtil;

import java.util.Date;
import java.util.UUID;

@Service
@Transactional
@NoArgsConstructor
public class SessionService implements ISessionService {

    @Autowired
    private @Nullable SpringSessionRepository sessionRepository;

    @Override
    @Nullable
    public SessionDTO open(@NotNull User user) {
        @Nullable SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setId(UUID.randomUUID().toString());
        sessionDTO.setUserId(user.getId());
        sessionDTO.setTimestamp(new Date().getTime());
        @NotNull final String salt = "SomeVerySpicySalt";
        @Nullable final String signature = SignatureUtil.sign(sessionDTO, salt, 100);
        if (signature == null || signature.isEmpty()) return null;
        sessionDTO.setSignature(signature);
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        sessionRepository.save(session);
        return sessionDTO;
    }

    @Override
    @Nullable
    public Result close(@NotNull final Session session) {
        sessionRepository.delete(session);
        return new Result();
    }

    @Override
    public void validate(@Nullable final Session session) throws AccessForbiddenException {
        if (session == null) throw new AccessForbiddenException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessForbiddenException();
        if (session.getTimestamp() == null) throw new AccessForbiddenException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessForbiddenException();
        @NotNull final String signatureSource = session.getSignature();
        SignatureUtil.sign(temp, "SomeVerySpicySalt", 100);
        @Nullable final String signatureTarget = temp.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessForbiddenException();
        if (!sessionRepository.findById(session.getId()).isPresent()) throw new AccessForbiddenException();
    }
}
