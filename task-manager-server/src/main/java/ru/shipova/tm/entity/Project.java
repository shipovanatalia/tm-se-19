package ru.shipova.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

import static javax.persistence.FetchType.EAGER;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "app_project")
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Project extends AbstractFieldOfUser implements Serializable {

    public static final long serialVersionUID = 1;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "name")
    @Nullable private String name;

    @Column(name = "description")
    @NotNull private String description = "";

   @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
   @Nullable private List<Task> taskList;
}
