package ru.shipova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.constant.Status;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public class AbstractFieldOfUser extends AbstractEntity implements Serializable {
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    @Nullable Status status;

    @Column(name = "dateCreate")
    @Nullable Date dateOfCreate;

    @Column(name = "dateBegin")
    @Nullable Date dateOfBegin;

    @Column(name = "dateEnd")
    @Nullable Date dateOfEnd;
}
