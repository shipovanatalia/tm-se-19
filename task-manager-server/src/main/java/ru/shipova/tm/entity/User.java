package ru.shipova.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.constant.RoleType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

import static javax.persistence.CascadeType.REMOVE;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "app_user")
@JsonIgnoreProperties(ignoreUnknown = true)
public final class User extends AbstractEntity implements Serializable {
    public static final long serialVersionUID = 1;

    @Column(unique = true)
    @Nullable
    private String login;

    @Column(name = "role")
    @Nullable
    @Enumerated(EnumType.STRING)
    private RoleType roleType;

    @Nullable
    private String passwordHash;

    @OneToMany(mappedBy = "user", cascade = REMOVE, orphanRemoval = true)
    private List<Project> projectList;

    @OneToMany(mappedBy = "user", cascade = REMOVE, orphanRemoval = true)
    private List<Task> taskList;

    @OneToMany(mappedBy = "user", cascade = REMOVE, orphanRemoval = true)
    private List<Session> sessionList;

    public User(@NotNull final String id, @Nullable final String login, @Nullable final String passwordHash, @Nullable final RoleType roleType) {
        this.id = id;
        this.login = login;
        this.roleType = roleType;
        this.passwordHash = passwordHash;
    }
}
