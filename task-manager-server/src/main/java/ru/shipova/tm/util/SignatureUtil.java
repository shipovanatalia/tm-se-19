package ru.shipova.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class SignatureUtil {
    @Nullable
    public static String sign(@NotNull final Object value, @NotNull final String salt, @NotNull final Integer cycle) {
        try {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writeValueAsString(value);
            return sign(json, salt, cycle);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    @Nullable
    public static String sign(@Nullable final String value, @Nullable final String salt, @NotNull final Integer cycle){
        if (value == null) return null;
        if (salt == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < cycle; i++) {
            result = HashUtil.md5(salt + result + salt);
        }
        return result;
    }
}
