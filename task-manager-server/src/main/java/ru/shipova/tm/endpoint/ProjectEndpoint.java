package ru.shipova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.dto.ProjectDTO;
import ru.shipova.tm.dto.SessionDTO;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.exception.AccessForbiddenException;
import ru.shipova.tm.exception.CommandCorruptException;
import ru.shipova.tm.util.EntityConvertUtil;

import javax.annotation.Nullable;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(name = "ProjectEndpoint", targetNamespace = "http://endpoint.tm.shipova.ru/")
public class ProjectEndpoint extends AbstractEndpoint {
    public ProjectEndpoint() {
        super(null);
    }

    public ProjectEndpoint(@Nullable final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @WebMethod
    public List<ProjectDTO> getAllProjectList(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return new ArrayList<>();
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        @Nullable final List<Project> projectList = serviceLocator.getIProjectService().getProjectListOfUser(session.getUser().getId());
        return EntityConvertUtil.projectListToProjectDTOList(projectList);
    }

    @WebMethod
    public void loadProjectList
            (@WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
             @WebParam(name = "projectDTOList", partName = "projectDTOList") @Nullable final List<ProjectDTO> projectDTOList
            ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        @Nullable final List<Project> projectList = EntityConvertUtil.projectDTOListToProjectList(projectDTOList);
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getIProjectService().load(projectList);
    }

    @Nullable
    @WebMethod
    public List<ProjectDTO> searchProject(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam (name = "partOfData", partName = "partOfData") @NotNull final String partOfData
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return new ArrayList<>();
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        @Nullable final List<Project> projectList = serviceLocator.getIProjectService().search(session.getUser().getId(), partOfData);
        return EntityConvertUtil.projectListToProjectDTOList(projectList);
    }

    @WebMethod
    public void createProject(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "projectName", partName = "projectName") @NotNull final String projectName
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getIProjectService().create(session.getUser().getId(), projectName);
    }

    @WebMethod
    public void clearProjectListOfUser(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getIProjectService().clear(session.getUser().getId());
    }

    @WebMethod
    public void removeProject(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "projectName", partName = "projectName") @Nullable final String projectName
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getIProjectService().remove(session.getUser().getId(), projectName);
    }
}
