package ru.shipova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.api.endpoint.ISessionEndpoint;
import ru.shipova.tm.dto.Result;
import ru.shipova.tm.dto.SessionDTO;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.AccessForbiddenException;
import ru.shipova.tm.util.EntityConvertUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(name = "SessionEndpoint", targetNamespace = "http://endpoint.tm.shipova.ru/")
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint() {
        super(null);
    }

    public SessionEndpoint(@Nullable final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    @Nullable
    public SessionDTO openSession(
            @WebParam(name = "user", partName = "user") @NotNull final User user) {
        if (serviceLocator == null) return null;
        return serviceLocator.getISessionService().open(user);
    }

    @Override
    @WebMethod
    @Nullable
    public Result closeSession(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return null;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        return serviceLocator.getISessionService().close(session);
    }
}
