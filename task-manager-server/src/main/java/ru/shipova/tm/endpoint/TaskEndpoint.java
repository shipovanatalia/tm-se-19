package ru.shipova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.dto.SessionDTO;
import ru.shipova.tm.dto.TaskDTO;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.exception.AccessForbiddenException;
import ru.shipova.tm.exception.CommandCorruptException;
import ru.shipova.tm.exception.ProjectDoesNotExistException;
import ru.shipova.tm.util.EntityConvertUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(name = "TaskEndpoint", targetNamespace = "http://endpoint.tm.shipova.ru/")
public class TaskEndpoint extends AbstractEndpoint{

    public TaskEndpoint() {
        super(null);
    }

    public TaskEndpoint(@Nullable final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @NotNull
    public List<TaskDTO> showAllTasksOfProject(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "projectName", partName = "projectName") @NotNull final String projectName
    ) throws AccessForbiddenException, ProjectDoesNotExistException {
        if (serviceLocator == null) return new ArrayList<>();
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        @Nullable final List<Task> taskList = serviceLocator.getITaskService().showAllTasksOfProject(projectName);
        return EntityConvertUtil.taskListToTaskDTOList(taskList);
    }

    @WebMethod
    @Nullable
    public List<TaskDTO> getTaskListOfUser(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return new ArrayList<>();
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        @Nullable final List<Task> taskList = serviceLocator.getITaskService().getTaskListOfUser(session.getUser().getId());
        return EntityConvertUtil.taskListToTaskDTOList(taskList);
    }

    @WebMethod
    @Nullable
    public List<TaskDTO> searchTask(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "partOfData", partName = "partOfData") final String partOfData
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return new ArrayList<>();
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        @Nullable final List<Task> taskList = serviceLocator.getITaskService().search(session.getUser().getId(), partOfData);
        return EntityConvertUtil.taskListToTaskDTOList(taskList);
    }

    @WebMethod
    public void createTask(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String taskName,
            @WebParam(name = "projectName", partName = "projectName") @Nullable final String projectName
    ) throws AccessForbiddenException, ProjectDoesNotExistException {
        if (serviceLocator == null) return;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getITaskService().create(session.getUser().getId(), taskName, projectName);
    }

    @WebMethod
    public void clearTaskListOfUser(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getITaskService().clear(session.getUser().getId());
    }

    @WebMethod
    public void removeTaskFromUser(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String taskName
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getITaskService().remove(session.getUser().getId(), taskName);
    }

    @WebMethod
    public void loadTaskList(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "taskDTOList", partName = "taskDTOList") @Nullable final List<TaskDTO> taskDTOList
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        List<Task> taskList = EntityConvertUtil.taskDTOListToTaskList(taskDTOList);
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getITaskService().load(taskList);
    }
}
