package ru.shipova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.dto.SessionDTO;
import ru.shipova.tm.dto.UserDTO;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.AccessForbiddenException;
import ru.shipova.tm.exception.LoginAlreadyExistsException;
import ru.shipova.tm.util.EntityConvertUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(name = "UserEndpoint", targetNamespace = "http://endpoint.tm.shipova.ru/")
public class UserEndpoint extends AbstractEndpoint {
    public UserEndpoint() {
        super(null);
    }

    public UserEndpoint(@Nullable final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public void registryUser(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "role", partName = "role") @Nullable final String role
    ) throws AccessForbiddenException, LoginAlreadyExistsException {
        if (serviceLocator == null) return;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getIUserService().registryUser(login, password, role);
    }

    @WebMethod
    public void updateUser(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "role", partName = "role") @Nullable final String role
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getIUserService().updateUser(login, role);
    }

    @WebMethod
    @Nullable
    public String getRoleType(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return null;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        @NotNull final String roleType = serviceLocator.getIUserService().getRoleType(session).displayName();
        return roleType;
    }

    @WebMethod
    @Nullable
    public String getUserLogin(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return null;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        @Nullable final User user = serviceLocator.getIUserService().findById(session.getUser().getId());
        if (user == null) return null;
        @Nullable final String login = user.getLogin();
        return login;
    }

    @WebMethod
    public void setNewPassword(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "userLogin", partName = "userLogin") @Nullable final String userLogin,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getIUserService().setNewPassword(userLogin, password);
    }

    @WebMethod
    @Nullable
    public SessionDTO authorize(
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) {
        if (serviceLocator == null) return null;
        @Nullable User user = serviceLocator.getIUserService().authorize(login, password);
        if (user == null) return null;
        @Nullable final SessionDTO sessionDTO = serviceLocator.getISessionService().open(user);
        return sessionDTO;
    }

    @WebMethod
    public void loadUserList(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "userDTOList", partName = "userDTOList") @NotNull final List<UserDTO> userDTOList
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        @NotNull final List<User> userList = EntityConvertUtil.userDTOListToUserList(userDTOList);
        serviceLocator.getIUserService().load(userList);
    }

    @WebMethod
    @Nullable
    public List<UserDTO> getUserList(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return new ArrayList<>();
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        @Nullable final List<User> userList = serviceLocator.getIUserService().getUserList();
        return EntityConvertUtil.userListToUserDTOList(userList);
    }
}
