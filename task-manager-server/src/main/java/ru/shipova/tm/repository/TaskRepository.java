package ru.shipova.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.shipova.tm.entity.Task;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@Repository
@NoArgsConstructor
public final class TaskRepository{

    @Nullable
    private EntityManager entityManager;

    public TaskRepository(@Nullable EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void persist(@NotNull final Task task){
        if (entityManager == null) return;
        entityManager.persist(task);
    }

    public void merge(@NotNull final Task task){
        if (entityManager == null) return;
        entityManager.merge(task);
    }

    @Nullable
    public Task findByTaskId(@NotNull final String id){
        if (entityManager == null) return null;
        return entityManager.find(Task.class, id);
    }

    public void removeByTaskId(@NotNull final String id){
        if (entityManager == null) return;
        @Nullable final Task task = entityManager.find(Task.class, id);
        if (task == null) return;
        entityManager.remove(task);
    }

    @NotNull
    public List<Task> showAllTasksOfProject(@NotNull final String projectId){
        if (entityManager == null) return new ArrayList<>();
        @NotNull final String query = "SELECT t FROM Task t WHERE t.project.id = :projectId";
        return entityManager.createQuery(query, Task.class)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @NotNull
    public List<Task> findAllByUserId(@NotNull final String userId){
        if (entityManager == null) return new ArrayList<>();
        @NotNull final String query = "SELECT t FROM Task t WHERE t.user.id = :userId";
        return entityManager.createQuery(query, Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    public void removeAllTasksOfProject(@NotNull final String projectId) {
        if (entityManager == null) return;
        @NotNull final List<Task> taskList = showAllTasksOfProject(projectId);
        for (Task task : taskList) {
            entityManager.remove(task);
        }
    }

    public void removeAllByUserId(@NotNull final String userId) {
        if (entityManager == null) return;
        @NotNull final List<Task> taskList = findAllByUserId(userId);
        for (Task task : taskList) {
            entityManager.remove(task);
        }
    }

    @Nullable
    public Task getTaskByName(@NotNull final String taskName){
        if (entityManager == null) return null;
        @NotNull final String query = "SELECT t FROM Task t WHERE t.name = :taskName";
        return entityManager.createQuery(query, Task.class)
                .setParameter("taskName", taskName)
                .getSingleResult();
    }

    @Nullable
    public List<Task> search(@NotNull final String userId,
                             @NotNull final String partOfData
    ) {
        if (entityManager == null) return null;
        @NotNull final String query = "SELECT t FROM Task t WHERE t.user.id=:userId AND " +
                "(t.name LIKE %:partOfData% OR t.description LIKE %:partOfData%)";
        return entityManager.createQuery(query, Task.class)
                .setParameter("userId", userId)
                .setParameter("partOfData", partOfData)
                .getResultList();
    }
}
