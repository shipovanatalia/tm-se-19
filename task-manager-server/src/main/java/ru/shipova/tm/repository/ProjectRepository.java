package ru.shipova.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.shipova.tm.entity.Project;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@NoArgsConstructor
public final class ProjectRepository {

    @Nullable
    private EntityManager entityManager;

    public ProjectRepository(@Nullable EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void persist(@NotNull final Project project){
        if (entityManager == null) return;
        entityManager.persist(project);
    }

    public void merge(@NotNull final Project project){
        if (entityManager == null) return;
        entityManager.merge(project);
    }

    public void removeByProjectId(@NotNull final String id){
        if (entityManager == null) return;
        @Nullable final Project project = entityManager.find(Project.class, id);
        if (project == null) return;
        entityManager.remove(project);
    }

    @Nullable
    public Project findByProjectId(@NotNull final String id){
        if (entityManager == null) return null;
        return entityManager.find(Project.class, id);
    }

    @Nullable
    public Project getProjectByName(@NotNull final String projectName){
        if (entityManager == null) return null;
        @NotNull final String query = "Select p from Project p where p.name = :projectName";
        return entityManager.createQuery(query, Project.class)
                .setParameter("projectName", projectName)
                .getSingleResult();
    }

    @Nullable
    public List<Project> getProjectListByUserId(@NotNull final String userId){
        if (entityManager == null) return null;
        @NotNull final String query = "Select p from Project p where p.user.id = :userId";
        return entityManager.createQuery(query, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    public void removeAllByUserId(@NotNull final String userId) {
        if (entityManager == null) return;
        @Nullable final List<Project> projectList = getProjectListByUserId(userId);
        if (projectList == null) return;
        for (Project project : projectList) {
            entityManager.remove(project);
        }
    }

    @Nullable
    public List<Project> search(@NotNull final String userId, @NotNull final String partOfData) {
        if (entityManager == null) return null;
        @NotNull final String query = "SELECT p FROM Project p WHERE p.user.id = :userId " +
                "AND (p.name LIKE %:partOfData% OR p.description LIKE %:partOfData%)";
        @Nullable final List<Project> projectList = entityManager.createQuery(query, Project.class)
                .setParameter("userId", userId)
                .setParameter("partOfData", partOfData)
                .getResultList();
        return projectList;
    }
}
