package ru.shipova.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.shipova.tm.entity.Session;

import javax.persistence.EntityManager;

@Repository
@NoArgsConstructor
public final class SessionRepository {

    @Nullable
    private EntityManager entityManager;

    public SessionRepository(@Nullable EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void persist(@NotNull final Session session){
        if (entityManager == null) return;
        entityManager.persist(session);
    }

    public void removeBySessionId(@NotNull final String id){
        if (entityManager == null) return;
        @Nullable final Session session = entityManager.find(Session.class, id);
        if (session == null) return;
        entityManager.remove(session);
    }

    @Nullable
    public Session findBySessionId(@NotNull final String id){
        if (entityManager == null) return null;
        return entityManager.find(Session.class, id);
    }
}
