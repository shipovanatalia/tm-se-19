package ru.shipova.tm.config;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.api.endpoint.ISessionEndpoint;
import ru.shipova.tm.endpoint.*;

import javax.sql.DataSource;
import javax.xml.ws.Endpoint;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "ru.shipova.tm.api.repository")
@PropertySource("classpath:application.properties")
public class SpringConfig {
    @Value("${jdbc.driver}")
    @Nullable private String driver;
    @Value("${db.login}")
    @Nullable private String username;
    @Value("${db.password}")
    @Nullable private String password;
    @Value("${db.host}")
    @Nullable private String url;
    @Value("${serverHost}")
    @Nullable private String serverHost;
    @Value("${serverPort}")
    @Nullable private Integer serverPort;

    @Bean
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource =
                new DriverManagerDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            final DataSource dataSource
    ) {
        final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.shipova.tm");
        final Properties properties = new Properties();
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.dialect",
                "org.hibernate.dialect.MySQL5Dialect");
        factoryBean.setJpaProperties(properties);
        return factoryBean;

    }
    @Bean
    public PlatformTransactionManager transactionManager(
            final LocalContainerEntityManagerFactoryBean emf
    ) {
        final JpaTransactionManager transactionManager =
                new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf.getObject());
        return transactionManager;
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @Nullable final String host = serverHost;
        @Nullable Integer port = serverPort;
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(wsdl, endpoint);
    }

    @Bean
    public ISessionEndpoint sessionEndpoint(@Nullable final IServiceLocator serviceLocator){
        @NotNull final SessionEndpoint sessionEndpoint = new SessionEndpoint(serviceLocator);
        registry(sessionEndpoint);
        return sessionEndpoint;
    }

    @Bean
    public ProjectEndpoint projectEndpoint(@Nullable final IServiceLocator serviceLocator){
        @NotNull final ProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);
        registry(projectEndpoint);
        return projectEndpoint;
    }

    @Bean
    public UserEndpoint userEndpoint(@Nullable final IServiceLocator serviceLocator){
        @NotNull final UserEndpoint userEndpoint = new UserEndpoint(serviceLocator);
        registry(userEndpoint);
        return userEndpoint;
    }

    @Bean
    public TaskEndpoint taskEndpoint(@Nullable final IServiceLocator serviceLocator){
        @NotNull final TaskEndpoint taskEndpoint = new TaskEndpoint(serviceLocator);
        registry(taskEndpoint);
        return taskEndpoint;
    }

    @Bean
    public DomainEndpoint domainEndpoint(@Nullable final IServiceLocator serviceLocator){
        @NotNull final DomainEndpoint domainEndpoint = new DomainEndpoint(serviceLocator);
        registry(domainEndpoint);
        return domainEndpoint;
    }

    @Bean
    public AdminUserEndpoint adminUserEndpoint(@Nullable final IServiceLocator serviceLocator){
        @NotNull final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(serviceLocator);
        registry(adminUserEndpoint);
        return adminUserEndpoint;
    }
}
