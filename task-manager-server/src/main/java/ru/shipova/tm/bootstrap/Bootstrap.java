package ru.shipova.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.api.endpoint.ISessionEndpoint;
import ru.shipova.tm.api.service.*;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.endpoint.*;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.LoginAlreadyExistsException;

import javax.persistence.EntityManagerFactory;
import java.util.List;

/**
 * Класс загрузчика приложения
 */
@Service
public final class Bootstrap implements IServiceLocator {
    @Autowired
    private EntityManagerFactory entityManagerFactory;
    @Autowired
    private IProjectService projectService;
    @Autowired
    private ITaskService taskService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IDomainService domainService;
    @Autowired
    private ISessionService sessionService;
    @Autowired
    private IAdminUserService adminUserService;
    @Autowired
    private ISessionEndpoint sessionEndpoint;
    @Autowired
    private ProjectEndpoint projectEndpoint;
    @Autowired
    private UserEndpoint userEndpoint;
    @Autowired
    private TaskEndpoint taskEndpoint;
    @Autowired
    private DomainEndpoint domainEndpoint;
    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    public void init() {
        @Nullable final List<User> userList;
        userList = userService.getUserList();
        if (userList == null || userList.isEmpty()) initDefaultUsers();
    }

    private void initDefaultUsers() {
        try {
            userService.createUser("user", "user", RoleType.USER);
            userService.createUser("admin", "admin", RoleType.ADMIN);
        } catch (LoginAlreadyExistsException e) {
            e.printStackTrace();
        }
    }

    @NotNull
    @Override
    public IProjectService getIProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getITaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IUserService getIUserService() {
        return userService;
    }

    @Override
    public @NotNull IDomainService getIDomainService() {
        return domainService;
    }

    @Override
    public @NotNull ISessionService getISessionService() {
        return sessionService;
    }

    @Override
    public @NotNull IAdminUserService getIAdminUserService() {
        return adminUserService;
    }

    @Override
    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }
}
