package ru.shipova.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.endpoint.*;
import ru.shipova.tm.exception.CommandCorruptException;
import ru.shipova.tm.service.SessionService;
import ru.shipova.tm.service.TerminalService;

import java.util.*;

/**
 * Класс загрузчика приложения
 */
@Service
public final class Bootstrap implements IServiceLocator {

    @Autowired
    @Nullable
    private TerminalService terminalService;
    @Autowired
    @Nullable
    private SessionService sessionService;
    @Autowired
    @Nullable
    private UserEndpoint userEndpoint;
    @Autowired
    @Nullable
    private ProjectEndpoint projectEndpoint;
    @Autowired
    @Nullable
    private TaskEndpoint taskEndpoint;
    @Autowired
    @Nullable
    private SessionEndpoint sessionEndpoint;
    @Autowired
    @Nullable
    private DomainEndpoint domainEndpoint;
    @Autowired
    @Nullable
    private AdminUserEndpoint adminUserEndpoint;

    //At runtime, find all classes in a Java application that extend a AbstractCommand class
    private final Set<Class<? extends AbstractCommand>> commandSet =
            new Reflections("ru.shipova.tm").getSubTypesOf(AbstractCommand.class);

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void init() {
        loadCommands();
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        @NotNull String commandName = "";
        while (!"exit".equals(commandName)) {
            commandName = terminalService.nextLine();
            if (!commands.containsKey(commandName)) {
                System.out.println("WRONG COMMAND. ENTER 'help' TO GET ALL AVAILABLE COMMANDS.");
            }
            try {
                execute(commandName);
            } catch (Exception e) {
                System.out.println("CANNOT DO COMMAND.");
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }

    private void loadCommands() {
        for (@NotNull final Class commandClass : commandSet) {
            try {
                if (AbstractCommand.class.isAssignableFrom(commandClass))
                    registryCommand((AbstractCommand) commandClass.newInstance());
            } catch (CommandCorruptException | InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private void registryCommand(@Nullable final AbstractCommand command) throws CommandCorruptException {
        if (command == null) throw new CommandCorruptException();
        @Nullable final String commandName = command.getName(); //Command Line Interface
        @Nullable final String cliDescription = command.getDescription();
        if (commandName == null || commandName.isEmpty()) throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty()) throw new CommandCorruptException();
        command.setServiceLocator(this);
        commands.put(commandName, command);
    }
    private void execute(@Nullable final String commandName) throws Exception {
        if (commandName == null || commandName.isEmpty()) return;

        @Nullable final AbstractCommand abstractCommand = commands.get(commandName);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    @NotNull
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @Override
    @NotNull
    public TerminalService getTerminalService() {
        return terminalService;
    }

    @Override
    @NotNull
    public SessionService getSessionService() {
        return sessionService;
    }

    @Override
    @Nullable
    public UserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    @Override
    @Nullable
    public  ProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @Override
    @Nullable
    public TaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @Override
    @Nullable
    public SessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @Override
    @Nullable
    public DomainEndpoint getDomainEndpoint() {
        return domainEndpoint;
    }

    @Override
    @Nullable
    public AdminUserEndpoint getAdminUserEndpoint() {
        return adminUserEndpoint;
    }
}
