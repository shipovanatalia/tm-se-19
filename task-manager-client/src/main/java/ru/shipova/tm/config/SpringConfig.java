package ru.shipova.tm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.shipova.tm.endpoint.*;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

@Configuration
public class SpringConfig {
    private static final String NS = "http://endpoint.tm.shipova.ru/";

    @Bean
    public UserEndpoint userEndpoint() throws MalformedURLException {
        final URL url = new URL("http://localhost:8180/UserEndpoint?wsdl");
        final QName name = new QName(NS, "UserEndpointService");
        final Service service = Service.create(url, name);
        return service.getPort(UserEndpoint.class);
    }

    @Bean
    public ProjectEndpoint projectEndpoint() throws MalformedURLException {
        final URL url = new URL("http://localhost:8180/ProjectEndpoint?wsdl");
        final QName name = new QName(NS, "ProjectEndpointService");
        final Service service = Service.create(url, name);
        return service.getPort(ProjectEndpoint.class);
    }

    @Bean
    public TaskEndpoint taskEndpoint() throws MalformedURLException {
        final URL url = new URL("http://localhost:8180/TaskEndpoint?wsdl");
        final QName name = new QName(NS, "TaskEndpointService");
        final Service service = Service.create(url, name);
        return service.getPort(TaskEndpoint.class);
    }

    @Bean
    public SessionEndpoint sessionEndpoint() throws MalformedURLException {
        final URL url = new URL("http://localhost:8180/SessionEndpoint?wsdl");
        final QName name = new QName(NS, "SessionEndpointService");
        final Service service = Service.create(url, name);
        return service.getPort(SessionEndpoint.class);
    }

    @Bean
    public DomainEndpoint domainEndpoint() throws MalformedURLException {
        final URL url = new URL("http://localhost:8180/DomainEndpoint?wsdl");
        final QName name = new QName(NS, "DomainEndpointService");
        final Service service = Service.create(url, name);
        return service.getPort(DomainEndpoint.class);
    }

    @Bean
    public AdminUserEndpoint adminUserEndpoint() throws MalformedURLException {
        final URL url = new URL("http://localhost:8180/AdminUserEndpoint?wsdl");
        final QName name = new QName(NS, "AdminUserEndpointService");
        final Service service = Service.create(url, name);
        return service.getPort(AdminUserEndpoint.class);
    }
}
