package ru.shipova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.endpoint.AccessForbiddenException_Exception;
import ru.shipova.tm.endpoint.AdminUserEndpoint;
import ru.shipova.tm.endpoint.SessionDTO;
import ru.shipova.tm.endpoint.UserEndpoint;

public final class UserSetPasswordCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "set-password";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Set new password.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            if (adminUserEndpoint == null) return;
            @Nullable final UserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
            if (userEndpoint == null) return;
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO();
            @Nullable final String roleOfCurrentUser;
            @Nullable String userLoginToUpdate;
            try {
                if (!adminUserEndpoint.haveAccessToUsualCommand(sessionDTO, needAuthorize())) {
                    System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                    return;
                }
                userLoginToUpdate = userEndpoint.getUserLogin(sessionDTO);
                roleOfCurrentUser = userEndpoint.getRoleType(sessionDTO);
                if (RoleType.ADMIN.displayName().equals(roleOfCurrentUser)) {
                    System.out.println("ENTER LOGIN OF USER TO UPDATE:");
                    userLoginToUpdate = serviceLocator.getTerminalService().nextLine();
                }
                System.out.println("ENTER NEW PASSWORD");
                @NotNull final String password1 = serviceLocator.getTerminalService().nextLine();
                System.out.println("REPEAT NEW PASSWORD");
                @NotNull final String password2 = serviceLocator.getTerminalService().nextLine();
                if (password1.equals(password2)) {
                    userEndpoint.setNewPassword(sessionDTO, userLoginToUpdate, password1);
                    System.out.println("[OK]");
                } else {
                    System.out.println("PASSWORDS DO NOT MATCH.");
                    execute();
                }
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            }
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
