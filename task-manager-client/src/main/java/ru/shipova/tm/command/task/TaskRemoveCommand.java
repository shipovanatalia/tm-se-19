package ru.shipova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.endpoint.AccessForbiddenException_Exception;
import ru.shipova.tm.endpoint.AdminUserEndpoint;
import ru.shipova.tm.endpoint.SessionDTO;
import ru.shipova.tm.endpoint.TaskEndpoint;

public final class TaskRemoveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            if (adminUserEndpoint == null) return;
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO();
            try {
                if (!adminUserEndpoint.haveAccessToUsualCommand(sessionDTO, needAuthorize())) {
                    System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                    return;
                }
                System.out.println("[TASK REMOVE]");
                System.out.println("ENTER NAME OF TASK:");
                @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();
                @Nullable final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
                if (taskEndpoint == null) return;
                taskEndpoint.removeTaskFromUser(sessionDTO, projectName);
                System.out.println("[TASK " + projectName + " REMOVED]");
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            }
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
