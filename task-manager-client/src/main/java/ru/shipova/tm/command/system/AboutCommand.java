package ru.shipova.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display information about application.";
    }

    @Override
    public void execute() {
        System.out.println("Build number is " + Manifests.read("AppVersion"));
    }

    @Override
    public boolean needAuthorize() {
        return false;
    }
}
