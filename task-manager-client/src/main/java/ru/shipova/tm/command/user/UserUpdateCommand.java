package ru.shipova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.endpoint.AccessForbiddenException_Exception;
import ru.shipova.tm.endpoint.AdminUserEndpoint;
import ru.shipova.tm.endpoint.SessionDTO;
import ru.shipova.tm.endpoint.UserEndpoint;

public final class UserUpdateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update role type of user.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            try {
                @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO();
                if (adminUserEndpoint == null) return;
                if (!adminUserEndpoint.haveAccessToUsualCommand(sessionDTO, needAuthorize())) {
                    System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                    return;
                }
                if (!adminUserEndpoint.haveAccessToAdminCommand(sessionDTO, isOnlyAdminCommand())) {
                    System.out.println("ACCESS DENIED. NEED ADMINISTRATOR RIGHTS.");
                    return;
                }
                System.out.println("[USER UPDATE]");
                @Nullable final UserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
                if (userEndpoint == null) return;

                System.out.println("ENTER LOGIN OF USER TO UPDATE:");
                @NotNull final String login = serviceLocator.getTerminalService().nextLine();

                System.out.println("ENTER NEW ROLE TYPE:");
                @NotNull final String newRoleType = serviceLocator.getTerminalService().nextLine();
                userEndpoint.updateUser(sessionDTO, login, newRoleType);
                System.out.println("[OK]");
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            }
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }

    @Override
    public boolean isOnlyAdminCommand() {
        return true;
    }
}
