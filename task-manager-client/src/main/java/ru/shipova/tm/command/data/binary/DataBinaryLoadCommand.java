package ru.shipova.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.constant.Serializer;
import ru.shipova.tm.endpoint.AccessForbiddenException_Exception;
import ru.shipova.tm.endpoint.AdminUserEndpoint;
import ru.shipova.tm.endpoint.DomainEndpoint;
import ru.shipova.tm.endpoint.SessionDTO;

public class DataBinaryLoadCommand extends AbstractCommand {
    @Override
    @Nullable
    public String getName() {
        return "bin-load";
    }

    @Override
    @Nullable
    public String getDescription() {
        return "Load data from binary file.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            @Nullable final DomainEndpoint domainEndpoint = serviceLocator.getDomainEndpoint();
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO();
            if (adminUserEndpoint == null) return;
            try {
                if (!adminUserEndpoint.haveAccessToUsualCommand(sessionDTO, needAuthorize())) {
                    System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                    return;
                }
                if (!adminUserEndpoint.haveAccessToAdminCommand(sessionDTO, isOnlyAdminCommand())) {
                    System.out.println("ACCESS DENIED. NEED ADMINISTRATOR RIGHTS.");
                    return;
                }
                System.out.println("[DATA BINARY LOAD]");
                if (domainEndpoint == null) return;
                @NotNull final String deserializer = Serializer.BINARY.displayName();
                domainEndpoint.deserialize(sessionDTO, deserializer);
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            }
//            @Nullable final Domain domain =
//                    binarySerializer.deserialize();
//            if (domain == null) return;
//            serviceLocator.getIDomainService().load(domain);
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }

    @Override
    public boolean isOnlyAdminCommand() {
        return true;
    }
}
