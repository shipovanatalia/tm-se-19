package ru.shipova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.endpoint.AccessForbiddenException_Exception;
import ru.shipova.tm.endpoint.AdminUserEndpoint;
import ru.shipova.tm.endpoint.SessionDTO;
import ru.shipova.tm.endpoint.UserEndpoint;

public final class UserProfileCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Get all information about user.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            @Nullable final UserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
            if (userEndpoint == null) return;
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO();
            try {
                if (adminUserEndpoint == null) return;
                if (!adminUserEndpoint.haveAccessToUsualCommand(sessionDTO, needAuthorize())) {
                    System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                    return;
                }
                @Nullable final String userLogin = userEndpoint.getUserLogin(sessionDTO);
                @Nullable final String roleType = userEndpoint.getRoleType(sessionDTO);
                System.out.println("[USER PROFILE]");
                System.out.println("USER LOGIN: " + userLogin);
                System.out.println("USER ROLE TYPE: " + roleType);
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            }
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
