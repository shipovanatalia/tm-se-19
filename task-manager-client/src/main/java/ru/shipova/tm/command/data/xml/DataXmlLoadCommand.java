package ru.shipova.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.constant.Serializer;
import ru.shipova.tm.constant.TypeOfSerialization;
import ru.shipova.tm.endpoint.AdminUserEndpoint;
import ru.shipova.tm.endpoint.DomainEndpoint;
import ru.shipova.tm.endpoint.SessionDTO;

public class DataXmlLoadCommand extends AbstractCommand {
    @Override
    public @Nullable String getName() {
        return "xml-load";
    }

    @Override
    public @Nullable String getDescription() {
        return "Load data from XML-file.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            @Nullable final DomainEndpoint domainEndpoint = serviceLocator.getDomainEndpoint();
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO();
            if (adminUserEndpoint == null) return;
            if (!adminUserEndpoint.haveAccessToUsualCommand(sessionDTO, needAuthorize())) {
                System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                return;
            }
            if (!adminUserEndpoint.haveAccessToAdminCommand(sessionDTO, isOnlyAdminCommand())) {
                System.out.println("ACCESS DENIED. NEED ADMINISTRATOR RIGHTS.");
                return;
            }
            if (domainEndpoint == null) return;
            System.out.println("[DATA XML LOAD]");
            System.out.println("CHOOSE TYPE OF DESERIALIZATION:");
            System.out.println("1. FasterXml;");
            System.out.println("2. Jax-B.");
            @NotNull final String input = serviceLocator.getTerminalService().nextLine();
            @NotNull final String typeOfDeserialization = input.toUpperCase();
            if (typeOfDeserialization.equals("1") ||
                    typeOfDeserialization.equals(TypeOfSerialization.FASTER_XML.displayName())) {
                @NotNull final String deserializer = Serializer.XML_FASTER_XML.displayName();
                domainEndpoint.deserialize(sessionDTO, deserializer);
            }
            if (typeOfDeserialization.equals("2") ||
                    typeOfDeserialization.equals(TypeOfSerialization.JAX_B.displayName())) {
                @NotNull final String deserializer = Serializer.XML_JAX_B.displayName();
                domainEndpoint.deserialize(sessionDTO, deserializer);
            }
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }

    @Override
    public boolean isOnlyAdminCommand() {
        return true;
    }
}
