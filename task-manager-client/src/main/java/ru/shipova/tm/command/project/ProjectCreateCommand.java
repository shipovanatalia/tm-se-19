package ru.shipova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.endpoint.AccessForbiddenException_Exception;
import ru.shipova.tm.endpoint.AdminUserEndpoint;
import ru.shipova.tm.endpoint.ProjectEndpoint;
import ru.shipova.tm.endpoint.SessionDTO;

public final class ProjectCreateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            try {
                @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
                if (adminUserEndpoint == null) return;
                @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO();
                if (!adminUserEndpoint.haveAccessToUsualCommand(sessionDTO, needAuthorize())) {
                    System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                    return;
                }
                System.out.println("[PROJECT CREATE]");
                System.out.println("ENTER NAME:");
                @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();
                @Nullable final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
                if (projectEndpoint == null) return;
                projectEndpoint.createProject(sessionDTO, projectName);
                System.out.println("[OK]");
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            }
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }


}
