package ru.shipova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.endpoint.*;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class ProjectListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            if (adminUserEndpoint == null) return;
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO();
            try {
                if (!adminUserEndpoint.haveAccessToUsualCommand(sessionDTO, needAuthorize())) {
                    System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                    return;
                }
                System.out.println("[PROJECT LIST]");
                @Nullable final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
                if (projectEndpoint == null) return;
                if (sessionDTO == null) return;
                @Nullable List<ProjectDTO> projectList = projectEndpoint.getAllProjectList(sessionDTO);
                if (projectList == null) return;
                sortProjects(projectList, sessionDTO);
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            }
        }
    }

    private void sortProjects(@NotNull final List<ProjectDTO> projectList, @NotNull final SessionDTO sessionDTO) {
        if (serviceLocator != null) {
            System.out.println("PLEASE CHOOSE TYPE OF SORT OF PROJECTS: ");
            System.out.println("1. DATE OF CREATE;");
            System.out.println("2. DATE OF BEGIN;");
            System.out.println("3. DATE OF END;");
            System.out.println("4. STATUS.");
            System.out.println("ENTER NAME OF SORT OR NO");
            @NotNull final String typeOfSort = serviceLocator.getTerminalService().nextLine();
            switch (typeOfSort) {
                case "1":
                case "DATE OF CREATE":
                    Collections.sort(projectList, new Comparator<ProjectDTO>() {
                        @Override
                        public int compare(ProjectDTO p1, ProjectDTO p2) {
                            return p1.getDateOfCreate().compare(p2.getDateOfCreate());
                        }
                    });
                    break;
                case "2":
                case "DATE OF BEGIN":
                    Collections.sort(projectList, new Comparator<ProjectDTO>() {
                        @Override
                        public int compare(ProjectDTO p1, ProjectDTO p2) {
                            return p1.getDateOfBegin().compare(p2.getDateOfBegin());
                        }
                    });
                    break;
                case "3":
                case "DATE OF END":
                    Collections.sort(projectList, new Comparator<ProjectDTO>() {
                        @Override
                        public int compare(ProjectDTO p1, ProjectDTO p2) {
                            return p1.getDateOfEnd().compare(p2.getDateOfEnd());
                        }
                    });
                    break;
                case "4":
                case "STATUS":
                    Collections.sort(projectList, new Comparator<ProjectDTO>() {
                        @Override
                        public int compare(ProjectDTO p1, ProjectDTO p2) {
                            return p1.getStatus().compareTo(p2.getStatus());
                        }
                    });
                    break;
                case "NO":
                    break;
                }

            int index = 1;
            if (projectList.isEmpty()) System.out.println("PROJECT LIST IS EMPTY");

            for (@Nullable final ProjectDTO project : projectList) {
                    if (project == null) return;
                    System.out.println(index++ + ". " + project.getName());
                }
                System.out.println();
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
