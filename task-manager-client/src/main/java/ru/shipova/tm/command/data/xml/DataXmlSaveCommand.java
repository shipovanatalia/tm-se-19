package ru.shipova.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.constant.Serializer;
import ru.shipova.tm.constant.TypeOfSerialization;
import ru.shipova.tm.endpoint.AdminUserEndpoint;
import ru.shipova.tm.endpoint.Domain;
import ru.shipova.tm.endpoint.DomainEndpoint;
import ru.shipova.tm.endpoint.SessionDTO;

public class DataXmlSaveCommand extends AbstractCommand {

    @Override
    public @Nullable String getName() {
        return "xml-save";
    }

    @Override
    public @Nullable String getDescription() {
        return "Save data to XML.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            if (adminUserEndpoint == null) return;
            @Nullable final DomainEndpoint domainEndpoint = serviceLocator.getDomainEndpoint();
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO();
            if (!adminUserEndpoint.haveAccessToUsualCommand(sessionDTO, needAuthorize())) {
                System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                return;
            }
            if (!adminUserEndpoint.haveAccessToAdminCommand(sessionDTO, isOnlyAdminCommand())) {
                System.out.println("ACCESS DENIED. NEED ADMINISTRATOR RIGHTS.");
                return;
            }
            if (domainEndpoint == null) return;
            if (sessionDTO == null) return;
            System.out.println("[DATA XML SAVE]");
            System.out.println("CHOOSE TYPE OF SERIALIZATION:");
            System.out.println("1. FasterXml;");
            System.out.println("2. Jax-B.");
            @NotNull final String input = serviceLocator.getTerminalService().nextLine();
            @NotNull final String typeOfSerialization = input.toUpperCase();
            @NotNull final Domain domain = new Domain();
            if (typeOfSerialization.equals("1") ||
                    typeOfSerialization.equals(TypeOfSerialization.FASTER_XML.displayName())) {
                @NotNull final String serializer = Serializer.XML_FASTER_XML.displayName();
                domainEndpoint.serialize(sessionDTO, serializer);
            }
            if (typeOfSerialization.equals("2") ||
                    typeOfSerialization.equals(TypeOfSerialization.JAX_B.displayName())) {
                @NotNull final String serializer = Serializer.XML_JAX_B.displayName();
                domainEndpoint.serialize(sessionDTO, serializer);
            }
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }

    @Override
    public boolean isOnlyAdminCommand() {
        return true;
    }
}
