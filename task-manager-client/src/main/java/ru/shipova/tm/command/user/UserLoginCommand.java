package ru.shipova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.endpoint.SessionDTO;
import ru.shipova.tm.endpoint.UserEndpoint;
import ru.shipova.tm.service.SessionService;

public final class UserLoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Login in task manager.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @Nullable final UserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
            if (userEndpoint == null) return;
            @NotNull final SessionService sessionService = serviceLocator.getSessionService();
            System.out.println("[USER LOGIN]");
            System.out.println("ENTER LOGIN:");
            @NotNull final String login = serviceLocator.getTerminalService().nextLine();
            System.out.println("ENTER PASSWORD:");
            @NotNull final String password = serviceLocator.getTerminalService().nextLine();
            @Nullable SessionDTO sessionDTO = userEndpoint.authorize(login, password);
            if (sessionDTO == null) {
                System.out.println("WRONG LOGIN OR PASSWORD");
                return;
            }
            sessionService.setSessionDTO(sessionDTO);
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return false;
    }
}
