package ru.shipova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.endpoint.*;

import java.util.List;

public class ProjectSearchCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-search";
    }

    @Override
    public String getDescription() {
        return "Search project by part of name.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            if (adminUserEndpoint == null) return;
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO();
            try {
                if (!adminUserEndpoint.haveAccessToUsualCommand(sessionDTO, needAuthorize())) {
                    System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                    return;
                }
                System.out.println("[PROJECT SEARCH]");
                @Nullable final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
                if (projectEndpoint == null) return;
                @NotNull final String partOfData = serviceLocator.getTerminalService().nextLine();
                @Nullable final List<ProjectDTO> projectList;
                projectList = projectEndpoint.searchProject(sessionDTO, partOfData);
                if (projectList == null || projectList.isEmpty()) {
                    System.out.println("PROJECTS ARE NOT FOUND");
                    return;
                }
                int index = 1;
                for (@NotNull final ProjectDTO project : projectList) {
                    System.out.println(index++ + ". " + project.getName());
                }
                System.out.println();
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            }
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
