package ru.shipova.tm.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.shipova.tm.endpoint.SessionDTO;

@Service
@Getter
@Setter
@RequiredArgsConstructor
public class SessionService {
    @Nullable private SessionDTO sessionDTO = null;
}
