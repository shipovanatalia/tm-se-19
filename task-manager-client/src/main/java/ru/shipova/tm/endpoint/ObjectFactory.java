
package ru.shipova.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.shipova.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AccessForbiddenException_QNAME = new QName("http://endpoint.tm.shipova.ru/", "AccessForbiddenException");
    private final static QName _HaveAccessToAdminCommand_QNAME = new QName("http://endpoint.tm.shipova.ru/", "haveAccessToAdminCommand");
    private final static QName _HaveAccessToAdminCommandResponse_QNAME = new QName("http://endpoint.tm.shipova.ru/", "haveAccessToAdminCommandResponse");
    private final static QName _HaveAccessToUsualCommand_QNAME = new QName("http://endpoint.tm.shipova.ru/", "haveAccessToUsualCommand");
    private final static QName _HaveAccessToUsualCommandResponse_QNAME = new QName("http://endpoint.tm.shipova.ru/", "haveAccessToUsualCommandResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.shipova.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AccessForbiddenException }
     * 
     */
    public AccessForbiddenException createAccessForbiddenException() {
        return new AccessForbiddenException();
    }

    /**
     * Create an instance of {@link HaveAccessToAdminCommand }
     * 
     */
    public HaveAccessToAdminCommand createHaveAccessToAdminCommand() {
        return new HaveAccessToAdminCommand();
    }

    /**
     * Create an instance of {@link HaveAccessToAdminCommandResponse }
     * 
     */
    public HaveAccessToAdminCommandResponse createHaveAccessToAdminCommandResponse() {
        return new HaveAccessToAdminCommandResponse();
    }

    /**
     * Create an instance of {@link HaveAccessToUsualCommand }
     * 
     */
    public HaveAccessToUsualCommand createHaveAccessToUsualCommand() {
        return new HaveAccessToUsualCommand();
    }

    /**
     * Create an instance of {@link HaveAccessToUsualCommandResponse }
     * 
     */
    public HaveAccessToUsualCommandResponse createHaveAccessToUsualCommandResponse() {
        return new HaveAccessToUsualCommandResponse();
    }

    /**
     * Create an instance of {@link SessionDTO }
     * 
     */
    public SessionDTO createSessionDTO() {
        return new SessionDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccessForbiddenException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shipova.ru/", name = "AccessForbiddenException")
    public JAXBElement<AccessForbiddenException> createAccessForbiddenException(AccessForbiddenException value) {
        return new JAXBElement<AccessForbiddenException>(_AccessForbiddenException_QNAME, AccessForbiddenException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HaveAccessToAdminCommand }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shipova.ru/", name = "haveAccessToAdminCommand")
    public JAXBElement<HaveAccessToAdminCommand> createHaveAccessToAdminCommand(HaveAccessToAdminCommand value) {
        return new JAXBElement<HaveAccessToAdminCommand>(_HaveAccessToAdminCommand_QNAME, HaveAccessToAdminCommand.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HaveAccessToAdminCommandResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shipova.ru/", name = "haveAccessToAdminCommandResponse")
    public JAXBElement<HaveAccessToAdminCommandResponse> createHaveAccessToAdminCommandResponse(HaveAccessToAdminCommandResponse value) {
        return new JAXBElement<HaveAccessToAdminCommandResponse>(_HaveAccessToAdminCommandResponse_QNAME, HaveAccessToAdminCommandResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HaveAccessToUsualCommand }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shipova.ru/", name = "haveAccessToUsualCommand")
    public JAXBElement<HaveAccessToUsualCommand> createHaveAccessToUsualCommand(HaveAccessToUsualCommand value) {
        return new JAXBElement<HaveAccessToUsualCommand>(_HaveAccessToUsualCommand_QNAME, HaveAccessToUsualCommand.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HaveAccessToUsualCommandResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shipova.ru/", name = "haveAccessToUsualCommandResponse")
    public JAXBElement<HaveAccessToUsualCommandResponse> createHaveAccessToUsualCommandResponse(HaveAccessToUsualCommandResponse value) {
        return new JAXBElement<HaveAccessToUsualCommandResponse>(_HaveAccessToUsualCommandResponse_QNAME, HaveAccessToUsualCommandResponse.class, null, value);
    }

}
