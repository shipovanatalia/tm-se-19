
package ru.shipova.tm.endpoint;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.2.7
 * 2019-11-23T20:31:06.219+03:00
 * Generated source version: 3.2.7
 */

@WebFault(name = "LoginAlreadyExistsException", targetNamespace = "http://endpoint.tm.shipova.ru/")
public class LoginAlreadyExistsException_Exception extends Exception {

    private ru.shipova.tm.endpoint.LoginAlreadyExistsException loginAlreadyExistsException;

    public LoginAlreadyExistsException_Exception() {
        super();
    }

    public LoginAlreadyExistsException_Exception(String message) {
        super(message);
    }

    public LoginAlreadyExistsException_Exception(String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public LoginAlreadyExistsException_Exception(String message, ru.shipova.tm.endpoint.LoginAlreadyExistsException loginAlreadyExistsException) {
        super(message);
        this.loginAlreadyExistsException = loginAlreadyExistsException;
    }

    public LoginAlreadyExistsException_Exception(String message, ru.shipova.tm.endpoint.LoginAlreadyExistsException loginAlreadyExistsException, java.lang.Throwable cause) {
        super(message, cause);
        this.loginAlreadyExistsException = loginAlreadyExistsException;
    }

    public ru.shipova.tm.endpoint.LoginAlreadyExistsException getFaultInfo() {
        return this.loginAlreadyExistsException;
    }
}
